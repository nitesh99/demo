var controllers = angular.module('controllers', ['$ghana', 'ngStorage', 'ngCookies']);

var finalStep = 5;

function jumpToPage(page, caller) {
    return;
    var currentPage = window.location.href;
    console.log("page = " + page + " & caller = " + caller);
    if (page == 0 && currentPage.includes("registration-step-one.html") == false) {
        window.location = "registration-step-one.html";
    } else if (page == 1 && currentPage.includes("registration-step-two.html") == false) {
        window.location = "registration-step-two.html";
    } else if (page == 2 && currentPage.includes("registration-step-three.html") == false) {
        window.location = "registration-step-three.html";
    } else if (page == 3 && currentPage.includes("registration-step-four.html") == false) {
        window.location = "registration-step-four.html";
    } else if (page == 4 && currentPage.includes("registration-step-five.html") == false) {
        window.location = "registration-step-five.html";
    }
}

controllers.controller('viewProfilesController', function ($scope, $rootScope, $cookies, $localStorage, $rest, $http, $q) {
    $scope.allUsers = {};
    $scope.user = [];
    $rest.get('user/view').then(function (response) {
        $scope.user = response.data;
        $scope.allUsers = response.data;
    });

    $scope.filterProfiles = function (gender) {
        $scope.user = [];
        if (gender == '') {
            $scope.user = $scope.allUsers;
        } else {
            angular.forEach($scope.allUsers, function (object) {
                if (gender == object.gender)
                    $scope.user.push(object);
            });
        }
    };

});


controllers.controller('bodyController', function ($scope, $rootScope, $cookies, $localStorage, $rest, $http, $q) {

    $scope.countrylist = null;

    $cookies.put('regStepCompleted', "");
    $scope.user = {};

    $rootScope.logedUserName = 'Login';
    var logedUserName = $cookies.get('logedUserName');

    if (logedUserName != undefined && logedUserName.length > 1) {
        $rootScope.logedUserName = logedUserName;


        var getUser = 'user/' + $cookies.get('logedUserId');
        $rest.get(getUser).then(function (response) {
            $scope.user = response.data;
            setUserCookies($cookies, $scope.user);

            jumpToPage($scope.user.regStepCompleted, 'bodyController');

            setTimeout(function () {
                $('.mdb-select').material_select('destroy');
                $('.mdb-select').material_select();
            }, 0);

        }, function (error) {
            console.log(JSON.stringify(error));
        });


    }

    $scope.logout = function () {
        $rootScope.logedUserName = 'Login';
        $cookies.put('logedUserName', "");
        $cookies.put('logedUserId', "");
    }

    $scope.masterData = {};

    var masterTableList = ["master_day", "master_month", "master_profile_created_for", "master_marital_status",
        "master_aged_from", "master_religion", "master_mother_tongue", "master_cast", "master_year", "master_height",
        "master_blood_group", "master_zodiac_sign", "master_gothra", "master_nakshatra", "master_education", "master_employment_sector",
        "master_employment_area", "master_occupation", "master_annual_income", "master_income_in", "master_hobbies", "master_music",
        "master_sports", "master_books", "master_spoken_languages", "master_fathers_name", "mother_name", "master_country", "master_state", "master_district"
    ];

    var arr = [];

    for (var i = 0; i < masterTableList.length; i++) {
        var responsePromise = $rest.get(masterTableList[i] + '/view');
        arr.push(responsePromise);
        responsePromise.success(function (data, status, headers, config) {
            var tableName = config.url.split('/').slice(-2)[0]
            $scope.masterData[tableName] = data;
        });
    }

    $q.all(arr).then(function (arrResponse) {
        $('.mdb-select').material_select('destroy');
        $('.mdb-select').material_select();
    });


    $scope.sendPassword = function () {
        var url = 'user/forgot-password?email=' + $scope.user.email;
        var data = {};
        $rest.get(url).then(function (response) {
            alert('Your password will be sent to your email.');
        }, function (error) {
            console.log(error.data.message);
        });
    }
    $scope.approveProfile = function () {
        var userId = getParameterByName('key', window.location);
        alert("hi " + userId);
        var userurl = 'user/' + userId;
        $rest.get(userurl).success(function (response) {
            $scope.user = response;
            if ($scope.user.emailValidated == false) {
                $scope.user.emailValidated = true;
                $rest.patch(userurl, $scope.user).then(function () {
                    alert("User is get approved...");
                });
            } else {
                alert("User is allready approved by another Admin...");
            }
        });

    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
});

controllers.controller('registrationController', function ($scope, $rest, $localStorage) {
    $scope.user = { regStepCompleted: 0 };
    $scope.candidateName = true;

    $scope.register = function () {
        var url = 'user/register?email=' + $scope.user.email + '&emailConfirmation=true';

        $rest.post(url, $scope.user).then(function (response) {
            $('#modal-register').modal('hide');
            $('#modal-login').modal('show');
        }, function (error) {
            alert("User already exist with this email id.");
        });
    };

    $scope.$watch('user.profile_created_for', function () {
        if ($scope.user.profile_created_for !== undefined && $scope.user.profile_created_for.name == "Self") {
            $scope.candidateName = false;
            $scope.user.firstName = $scope.user.loginUserFirstName;
            $scope.user.lastName = $scope.user.loginUserLastName;
        } else {
            $scope.candidateName = true;
            $scope.user.firstName = "";
            $scope.user.lastName = "";
        }
    });

}); //eof registrationController

controllers.controller('loginController', function ($scope, $rootScope, $rest, $localStorage, $cookies) {

    $scope.user = {};
    $scope.login = function () {
        var condition = {
            "condition": [{
                "isOr": false,
                "searchpath": "email",
                "values": [
                    $scope.user.email
                ]
            }, {
                "isOr": false,
                "searchpath": "password",
                "values": [
                    $scope.user.password
                ]
            }],
            "sortAscending": true,
            "sortOn": "loginUserFirstName"
        };

        $rest.post('user/search', condition).then(function (response) {
            var user = response.data[0];
            if (user == undefined) {
                alert("Invalid username or password, Please try again.");
            } else {
                $('#modal-login').modal('hide')
                $('#hideLogin').hide();
                $('#hideRegistration').hide();
                $rootScope.logedUserName = user.loginUserFirstName;

                setUserCookies($cookies, user);

                jumpToPage(user.regStepCompleted, 'login');
            }
        });
    };
}); //eof loginController


controllers.controller('updateProfileController', function ($scope, $rootScope, $rest, $cookies, Upload, $timeout) {

    $scope.upload = function (dataUrl, name) {
        Upload.upload({
            url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
            data: {
                file: Upload.dataUrltoBlob(dataUrl, name)
            },
        }).then(function (response) {
            $timeout(function () {
                $scope.result = response.data;
            });
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status
                + ': ' + response.data;
        }, function (evt) {
            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
    }

    $scope.user = {};

    var getUser = 'user/' + $cookies.get('logedUserId');
    $rest.get(getUser).then(function (response) {
        $scope.user = response.data;
        //$scope.setValues();
        setTimeout(function () {
            $('.mdb-select').material_select('destroy');
            $('.mdb-select').material_select();
        }, 0);

    }, function (error) {
        console.log(JSON.stringify(error));
    });

    $scope.showme = false;
    $scope.showSpectacle = function () {
        $scope.showme = !$scope.showme;
    };

    // $scope.$watch('user.marriedStatus', function() {
    //     if ($scope.user.marriedStatus == undefined || ($scope.user.marriedStatus.length > 1 && $scope.user.marriedStatus == 'Never Married')) {
    //         $(".Divorced").hide();
    //         $(".Divorced").hide();
    //     }
    // });

    // $scope.showLegal = false;
    // $scope.selectedMarriedStatus = function() {
    //     if ($scope.user.marital_status == 'Never Married') {
    //         $scope.showLegal = !$scope.showLegal;
    //     }
    // }

    $scope.updateProfile = function (step, flow) {
        if (step != undefined)
            $scope.user.regStepCompleted = step;

        console.log('$scope.user.regStepCompleted = ' + $scope.user.regStepCompleted);

        var updateUser = 'user/' + $cookies.get('logedUserId');
        $rest.patch(updateUser, $scope.user).then(function (response) {
            $scope.user = response.data;

            console.log(" " + JSON.stringify($scope.user));

            if (step != undefined && step != finalStep) { // Right now we have total 3 steps
                jumpToPage($scope.user.regStepCompleted, 'updateProfile');
            } else if (step == finalStep && flow != 'updateProfile') {
                window.location = "/";
            } else if (flow == 'updateProfile') {
                alert("Your profile has been updated");
            }

        }, function (error) {
            console.log(JSON.stringify(error));
        });

    }

}); //eof updateProfileController