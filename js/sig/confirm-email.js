 var app = angular.module('app', ['$ghana', 'ngCookies']);
 
app.controller('confirmAccount', function($scope, $rest, $cookies, $q) {
    var token = getParameterByName('token', window.location); //location.search.split('id=')[1];
    var userEmail = getParameterByName('email', window.location);
    var userId = getParameterByName('key', window.location);
    var flow = getParameterByName('flow', window.location);

    $scope.msg = "Please wait while we confirm your email address..."

    var url = 'user/' + userId;
    $rest.get(url).then(function(response) {
    	var url = 'user/confirm-registration/' + token;

	    $rest.get(url).then(function(response) {
	        var data = {
	            "mailSubject": "Your email has been confirmed, Thanks !",
	            "mailTo": [
	                userEmail
	            ],
	            "templateName": "registration-confirm",
	            "templateValues": {
	                "username": userEmail
	            }
	        }

	        $rest.post('_email_templates/send-email-use-db-template', data).then(function(responseOnTemplateSent) {});

	        $rest.get('user_admin/view').success(function(response) {
	                $scope.msg = "Your email has been confirmed, Thanks !";
	                var admin = response;
	                var arr = [];
	                for (var x in admin) {
	                    if (admin.hasOwnProperty(x)) {
	                        var data = {
	                            "mailSubject": "New user has been registered | Pending for approval",
	                            "mailTo": [
	                                admin[x].email
	                            ],
	                            "templateName": "registration-confirm",
	                            "templateValues": {
	                                "username": admin[x].email
	                            }
	                        }
	                        var responsePromise = $rest.post('_email_templates/send-email-use-db-template', data).then(function(response) {

	                        });

	                        arr.push(responsePromise);
	                    }
	                }

	                $q.all(arr).then(function(arrResponse) {
	                    setTimeout(function() {
	                        window.location = "/"
	                    }, 0);
	                });

	            },
	            function(error) {alert("Somthing went wrong ");});
	    },function(error) {alert("Somthing went wrong ");});
    }, function(error) {alert("Somthing went wrong ");});

}); //eof controller 