/**
 * 
 */

controllers.config(['$routeProvider', '$locationProvider', function AppConfig($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/home/landing-page.html'
        })
        .when('/registration', {
            redirectTo: '/registration/basic-information'
        })
        .when('/profile', {
            templateUrl: '/profile/kp_profile-page.html'
        })
        .when('/profile_list', {
            templateUrl: '/home/profile_list.html'
        })
        .when('/registration/basic-information', {
            templateUrl: '/registration/kp_1-basic-info.html'
        })
        .when('/registration/education-profession', {
            templateUrl: '/registration/kp_2-education-profession.html'
        })
        .when('/registration/lifestyle-appearance', {
            templateUrl: '/registration/kp_3-lifestyle-appearance.html'
        })
        .when('/registration/family-details', {
            templateUrl: '/registration/kp_4-family-details.html'
        })
        .when('/registration/describe-yourself', {
            templateUrl: '/registration/kp_5-about-yourself.html'
        })
        .when('/registration/verify-mobile-number', {
            templateUrl: '/registration/kp_6-mobile-verified.html'
        })
        .when('/registration/upload-profile-photos', {
            templateUrl: '/registration/kp_7-profile-photo.html'
        })
        .when('/registration/upload-kyc-documents', {
            templateUrl: '/registration/kp_8-kyc-doc.html'
        })
        .when('/registration/partner-preference', {
            templateUrl: '/registration/kp_9-partner-preference.html'
        })
		
		// profile
        .when('/profile/', {
        	templateUrl: '/profile/kp_profile-page.html'
        })
        .when('/profile/view', {
            templateUrl: '/profile/kp_profile-details.html'
        })
        .when('/profile/view/:profileId', {
            templateUrl: function(params){ return '/profile/kp_profile-details-readonly.html?'+params.profileId; }
        })
        .when('/profile/view/:profileId', {
            templateUrl: function(params){ return '/profile/kp_profile-details-readonly.html?'+params.profileId; }
        })
        .when('/profile/edit', {
            templateUrl: '/profile/kp_profile-edit.html'
        })
        .when('/profile/partner-preference', {
            templateUrl: '/profile/kp-partner-preference.html'
        })
        .when('/profile/upload-profile-photos', {
            templateUrl: '/profile/kp_profile-photo.html'
        })
		
		//setting
        .when('/profile/settings/privacy-setting', {
            templateUrl: '/settings/kp_privacy-setting.html'
        })
		
		//Inbox
        .when('/profile/inbox/received-messages', {
            templateUrl: '/inbox/kp_received-messages.html'
        })
		
		//Activity
        .when('/profile/activity/profile-visitor', {
            templateUrl: '/activity/kp_profile-visitor.html'
        })
		
        // removed other routes ... *snip
        .otherwise({
            redirectTo: '/'
        });

    // enable html5Mode for pushstate ('#'-less URLs)
    $locationProvider.html5Mode(true);
    /*$locationProvider.html5Mode({
	  enabled: true
	});*/
}]);