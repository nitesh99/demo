$(function() {
    $("select").change(function() {
        var isMultiple = false;
        var select = $(this);
        var modelName = $(this).attr('ng-model') == undefined ? $(this).attr('data-ng-model') : $(this).attr('ng-model');
        //console.log('modelName - ' + modelName);
        var masterDataAttr = $(this).attr('master-data');
        //console.log('masterDataAttr - ' + masterDataAttr);
        var value = [];
        if (select.attr('multiple') !== undefined && select.attr('multiple') === 'multiple') {
            //console.log('select.attr(multiple) - ' + $(this).val());
            value = ($(this).val() + "").split(",");
            isMultiple = true;
        } else {
            value[0] = $(this).val();
        }
        //console.log('value - ' + value);
        var scope = angular.element($('[data-ng-controller="bodyController"]')).scope();
        scope.$apply(function() {
            //masterData = scope.$eval('masterData');
            ////console.log('masterData = ' + JSON.stringify(masterData));
            masterData = scope.$eval(masterDataAttr);
            //console.log('masterData = ' + JSON.stringify(masterData));
            var modelValuesArrs = [];
            var modelValue;

            var scopeOfElement = angular.element($('[data-ng-model="' + modelName + '"]')).scope();
            var expression;

            angular.forEach(masterData, function(masterDataObject) {

                if (!isMultiple && masterDataObject._id == value[0]) {
                    modelValue = masterDataObject;
                } else if (isMultiple) {
                    for (var index = 0; index <= value.length; index++) {
                        if (masterDataObject._id == value[index]) {
                            modelValuesArrs.push(masterDataObject);
                        }
                    }
                }


            });
            if (isMultiple) {
                expression = modelName + " = " + JSON.stringify(modelValuesArrs);
            } else {
                expression = modelName + " = " + JSON.stringify(modelValue);
            }

            scopeOfElement.$eval(expression);
            //console.log("expression = " + expression);
        });
    });

    setTimeout(function() {
        $('.mdb-select').material_select('destroy');
        $('.mdb-select').material_select();
    }, 0);

});