/**
 * 
 */

$(function() {
	$('.easy-pie').easyPieChart({
		lineWidth : 3,
		scaleColor : false,
		size : 150,
		trackColor : '#00a6bb',
		barColor : 'white'
	});
});

/*----------------------
    Dialogs
 -----------------------*/

// Private Message
$('.PrivateMessage').click(function() {
	swal({
		title : "Message send successfully!",
		text : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		timer : 2000,
		type : 'success',
		showConfirmButton : false
	});
});
// Shortlist Message
$('.Shortlist').click(function() {
	swal({
		title : "Profile Shortlisted Successfully!",
		text : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		timer : 2000,
		type : 'success',
		showConfirmButton : false
	});
});
// Send Interest Message
$('.Interest').click(function() {
	swal({
		title : "Your Invitation Send Successfully!",
		text : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		timer : 2000,
		type : 'success',
		showConfirmButton : false
	});
});
// Profile Like Message
$('.like').click(function() {
	swal({
		title : "Profile added Successfully!",
		text : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		timer : 2000,
		type : 'success',
		showConfirmButton : false
	});
});
