var app = angular.module('app', ['$ghana', 'ngCookies']);

app.controller('changeemailconfirm', function($scope, $rest, $cookies, $q) {
    var token = getParameterByName('token', window.location); //location.search.split('id=')[1];
    var userEmail = getParameterByName('email', window.location);
    var userId = getParameterByName('key', window.location);
    var flow = getParameterByName('flow', window.location);

    $scope.msg = "Please wait while we confirm your email address..."

    var url = 'user/' + userId;
    $rest.get(url).then(function(response) {
        var user = response.data;
        if (token == user.emailVerificationToken) {
            var data = {
                "email": user.newEmail,
                "emailValidated": "true"
            }
            $rest.patch(url, data).then(function(response) {
            	user = response.data;
            	$scope.msg = "Your email has been confirmed, Thanks ! Please use your new email address for login.";
            	setUserCookies($cookies, user);
            }, function(error) {
                alert(JSON.stringify(error));
            });
            
	        var data = {
		            "mailSubject": "Your email has been confirmed, Thanks !",
		            "mailTo": [
		                 user.newEmail
		            ],
		            "templateName": "registration-confirm",
		            "templateValues": {
		                "username": user.newEmail
		            }
		        }

		        $rest.post('_email_templates/send-email-use-db-template', data).then(function(responseOnTemplateSent) {});

	        
        } else {
            alert('Invalid token recieved');
        }

    }, function(error) {
        alert(JSON.stringify(error));
    });
    
}); //eof controller

