var versionString = "?v11";

materialAdmin
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider){
        $urlRouterProvider.otherwise("/home");
        
        // Selecting default state
        $urlRouterProvider.when('/dashboard','/dashboard/user');

        $stateProvider
		
            //------------------------------
            // Error page
            //------------------------------			
			
            .state ('error', {
                url: '/error',
                templateUrl: '404.html'+versionString,
            })
			
		
            //------------------------------
            // Landing page
            //------------------------------			
			
            .state ('home', {
                url: '/home',
                templateUrl: 'view/home/landing-page.html'+versionString,
                controller: "bodyController"
            })
			
		
            //------------------------------
            // Cart
            //------------------------------			
			
            .state ('membership', {
                url: '/membership',
                templateUrl: 'view/membership/common.html'+versionString,
                controller: "bodyController"
            })	
			
            .state ('membership.plan', {
                url: '/plans',
                templateUrl: 'view/membership/membership-plan.html'+versionString
            })
			
            .state ('membership.cart', {
                url: '/cart/:plan',
                templateUrl: 'view/membership/cart.html'+versionString
            })
			
		
            //------------------------------
            // Inner page
            //------------------------------			
			
            .state ('index', {
                url: '',
                templateUrl: 'view/pages/common.html'+versionString
            })	
			
            .state ('section', {
                url: '',
                templateUrl: 'view/pages/section/common.html'+versionString
            })	
			
            .state ('section.about-us', {
                url: '/about-us',
                templateUrl: 'view/pages/about-us.html'+versionString
            })
			
            .state ('section.membership-plan', {
                url: '/membership-plan',
                templateUrl: 'view/pages/membership-plan.html'+versionString,
                controller: "bodyController"
            })
			
            .state ('section.partner-search', {
                url: '/partner-search',
                templateUrl: 'view/pages/partner-search.html'+versionString,
                controller: "bodyController"
            })
			
            .state ('section.search-result', {
                url: '/search-result',
                templateUrl: 'view/pages/search-result-page.html'+versionString
            })
				
			
            .state ('section.how-it-works', {
                url: '/how-it-works',
                templateUrl: 'view/pages/how-it-works.html'+versionString
            })
        
            .state ('index.profile-list', {
                url: '/profile-list',
                templateUrl: 'view/pages/profile_list.html'+versionString,
                controller: "commonController"
            })
        
            .state ('section.tnc', {
                url: '/terms-and-conditions',
                templateUrl: 'view/pages/section/terms-conditions.html'+versionString
            })
        
            .state ('section.privacy-policy', {
                url: '/privacy-policy',
                templateUrl: 'view/pages/section/privacy-policy.html'+versionString
            })
        
            .state ('section.career', {
                url: '/career',
                templateUrl: 'view/pages/section/career.html'+versionString
            })	
        
            .state ('section.needs-help', {
                url: '/needs-help',
                templateUrl: 'view/pages/section/needs-help.html'+versionString
            })
        
            .state ('section.contact-us', {
                url: '/contact-us',
                templateUrl: 'view/pages/section/contact-us.html'+versionString,
                controller: "commonController"
            })	
        
            .state ('section.online-safety', {
                url: '/online-safety',
                templateUrl: 'view/pages/section/be-safe-online.html'+versionString
            })	
        
            .state ('section.report-misuse', {
                url: '/report-misuse',
                templateUrl: 'view/pages/section/report-misuse.html'+versionString
            })	
        
            .state ('section.who-are-we', {
                url: '/who-are-we',
                templateUrl: 'view/pages/section/who-are-we.html'+versionString
            })	
        
            .state ('section.success-stories', {
                url: '/success-stories',
                templateUrl: 'view/pages/stories/stories-list.html'+versionString
            })
        
            .state ('section.my-story1', {
                url: '/success-stories/Sarita-&-Mahesh',
                templateUrl: 'view/pages/stories/my-story.html'+versionString
            })
        
            .state ('section.my-story2', {
                url: '/success-stories/Amruta-&-Yuvraj',
                templateUrl: 'view/pages/stories/my-story2.html'+versionString
            })
        
            // .state ('section.my-story', {
                // url: '/success-stories/my-story',
                // templateUrl: 'view/pages/stories/my-story.html'+versionString
            // })
        
            .state ('section.submit-story', {
                url: '/success-stories/submit-story',
                templateUrl: 'view/pages/stories/submit-story.html'+versionString
            })	
		
		
            //------------------------------
            // settings
            //------------------------------
        
            .state ('settings', {
                url: '/settings',
                templateUrl: 'view/settings/common.html'+versionString,
                controller: "commonController"
            })
        
            .state ('settings.account-settings', {
                url: '/account-settings',
                templateUrl: 'view/settings/kp_account-settings.html'+versionString
            })
        
            .state ('settings.privacy-settings', {
                url: '/privacy-settings',
                templateUrl: 'view/settings/kp_profile-settings.html'+versionString
            })
        
            .state ('settings.delete-account', {
                url: '/delete-account',
                templateUrl: 'view/settings/kp_delete-settings.html'+versionString
            })
        
            .state ('settings.contact-filter', {
                url: '/contact-filter',
                templateUrl: 'view/settings/kp_contact-filter.html'+versionString,
                controller: "locationController"
            })
        
            .state ('settings.alert-settings', {
                url: '/email-and-alert-settings',
                templateUrl: 'view/settings/kp_alert-settings.html'+versionString
            })
		
		
            //------------------------------
            // FAQs
            //------------------------------
        
            .state ('faqs', {
                url: '/faqs',
                templateUrl: 'view/pages/faqs/common.html'+versionString
            })
        
            .state ('faqs.registration', {
                url: '/registration',
                templateUrl: 'view/pages/faqs/faqs-registration.html'+versionString
            })
        
            .state ('faqs.login', {
                url: '/login',
                templateUrl: 'view/pages/faqs/faqs-login.html'+versionString
            })
        
            .state ('faqs.profile', {
                url: '/profile',
                templateUrl: 'view/pages/faqs/faqs-profile.html'+versionString
            })
        
            .state ('faqs.photographs', {
                url: '/photographs',
                templateUrl: 'view/pages/faqs/faqs-photographs.html'+versionString
            })
        
            .state ('faqs.profile-search', {
                url: '/profile-search',
                templateUrl: 'view/pages/faqs/faqs-profile-search.html'+versionString
            })
        
            .state ('faqs.messaging', {
                url: '/messaging',
                templateUrl: 'view/pages/faqs/faqs-messaging.html'+versionString
            })
        
            .state ('faqs.contacting-members', {
                url: '/contacting-members',
                templateUrl: 'view/pages/faqs/faqs-contacting-members.html'+versionString
            })
        
            .state ('faqs.membership', {
                url: '/membership',
                templateUrl: 'view/pages/faqs/faqs-membership.html'+versionString
            })
        
            .state ('faqs.payment-options', {
                url: '/payment-options',
                templateUrl: 'view/pages/faqs/faqs-payment.html'+versionString
            })
        
            .state ('faqs.notifications', {
                url: '/notifications',
                templateUrl: 'view/pages/faqs/faqs-notification.html'+versionString
            })
        
            .state ('faqs.technical-assistance', {
                url: '/technical-assistance',
                templateUrl: 'view/pages/faqs/faqs-tech-assistance.html'+versionString
            })
        
            .state ('faqs.online-safety', {
                url: '/online-safety',
                templateUrl: 'view/pages/faqs/faqs-online-safety.html'+versionString
            })
        
            .state ('faqs.feedback', {
                url: '/feedback-and-suggestions',
                templateUrl: 'view/pages/faqs/faqs-feedback.html'+versionString
            })
		
		
            //------------------------------
            // Registration
            //------------------------------			
			
            .state ('registration', {
                url: '/registration',
                /*views: {
                    '': { templateUrl: 'index.html'+versionString},
                    'nav@home': { templateUrl: './templates/assets/nav.html'+versionString },
                    'body@home': { templateUrl: 'view/registration/kp_1-basic-info.html'+versionString},
                    'footer@home': { templateUrl: './templates/assets/footer.html'+versionString }
                 }*/
                templateUrl: 'view/registration/common.html'+versionString,
                controller: "bodyController"
                
            })		
			
            .state ('registration.basic-information', {
                url: '/basic-information',
                templateUrl: 'view/registration/kp_1-basic-info.html'+versionString
            })
			
            .state ('registration.education-profession', {
                url: '/education-profession',
                templateUrl: 'view/registration/kp_2-education-profession.html'+versionString
            })
			
            .state ('registration.lifestyle-appearance', {
                url: '/lifestyle-appearance',
                templateUrl: 'view/registration/kp_3-lifestyle-appearance.html'+versionString 
            })
			
            .state ('registration.family-details', {
                url: '/family-details',
                templateUrl: 'view/registration/kp_4-family-details.html'+versionString 
            })
			
            .state ('registration.about-yourself', {
                url: '/about-yourself',
                templateUrl: 'view/registration/kp_5-about-yourself.html'+versionString 
            })
			
            .state ('registration.mobile-verified', {
                url: '/mobile-verified',
                templateUrl: 'view/registration/kp_6-mobile-verified.html'+versionString 
            })
			
            .state ('registration.profile-photo', {
                url: '/profile-photo',
                templateUrl: 'view/registration/kp_7-profile-photo.html'+versionString 
            })
			
            .state ('registration.add-kyc-document', {
                url: '/add-kyc-document',
                templateUrl: 'view/registration/kp_8-kyc-doc.html'+versionString 
            })
			
            .state ('registration.partner-preference', {
                url: '/partner-preference',
                templateUrl: 'view/registration/kp_9-partner-preference.html'+versionString 
            })
			
			
			
			
            //------------------------------
            // Dashboard
            //------------------------------
			
            .state ('dashboard', {
                url: '/dashboard',
                templateUrl: 'view/dashboard/common.html?v2',
                controller: "bodyController"
            })
            .state ('dashboard.default', {
                url: '/user',
                templateUrl: 'view/dashboard/kp_profile-page.html?v1',
            })
             .state ('dashboard.all-mutual-interest', {
                url: '/all-mutual-interest',
                templateUrl: 'view/dashboard/all-mutual-interest.html'+versionString 
            })
            .state ('dashboard.all-matching-profiles', {
                url: '/all-matching-profiles',
                templateUrl: 'view/dashboard/all-matching-profiles.html'+versionString 
            })
            .state ('dashboard.all-profile-visitors', {
                url: '/all-profile-visitors',
                templateUrl: 'view/dashboard/all-profile-visitors.html'+versionString 
            })
			
			
            //------------------------------
            // Profile
            //------------------------------
			.state ('profile', {
                url: '/profile',
                templateUrl: 'view/profile/common.html'+versionString,
                controller: "bodyController",
                resolve: {
                    loadPlugin: function($ocLazyLoad) {
                        return $ocLazyLoad.load ([
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js'
                                ]
                            }
                        ]);
                    }
                }
            
            })
            .state ('profile.profile-view', {
                url: '/view',
                templateUrl: 'view/profile/kp_profile-details.html'+versionString 
            })
			
            .state ('profile.view-kundali', {
                url: '/view-kundali',
                templateUrl: 'view/profile/kp_view-kundali.html?v1' 
            })
			
            .state ('profile.profile-edit', {
                url: '/edit',
                templateUrl: 'view/profile/kp_profile-edit.html'+versionString 
            })
			
            .state ('profile.mobile-verified', {
                url: '/mobile-verified',
                templateUrl: '/view/profile/section/mobile-verified.html'+versionString 
            })
			
            .state ('profile.kyc-upload', {
                url: '/kyc-upload',
                templateUrl: 'view/profile/kp_kyc-upload.html'+versionString 
            })
			
            .state ('profile.update-profile-photo', {
                url: '/update-photo',
                templateUrl: 'view/profile/kp_profile-photo.html'+versionString 
            })
			
            .state ('profile.update-partner-preference', {
                url: '/partner-preference',
                templateUrl: 'view/profile/kp-partner-preference.html'+versionString 
            })
			
            .state ('profile.search', {
                url: '/search',
                templateUrl: 'view/profile/kp_profile-page_search.html'+versionString 
            })
			
            
            .state ('profile.view', {
                url: '/view/:profileId',
                templateUrl: 'view/profile/kp_profile-details-readonly.html'+versionString,
                controller: "viewOtherProfileController"
            })
            
            //------------------------------
            // Search
            //------------------------------

            .state ('search', {
                url: '/search',
                abstract: true,
                templateUrl: 'view/search/common.html'+versionString,
                controller: "bodyController"
            })
			
            .state ('search.basic', {
                url: '/basic',
                templateUrl: 'view/search/kp_basic-search.html'+versionString
            })
			
            .state ('search.advanced', {
                url: '/advanced',
                templateUrl: 'view/search/kp_advanced-search.html'+versionString
            })
            
            //------------------------------
            // soulmate
            //------------------------------
            .state ('soulmate', {
                url: '/soulmate',
                abstract: true,
                templateUrl: 'view/soulmate/common.html'+versionString,
                controller: "soulmateController"
            })
             .state ('soulmate.find', {
            	url: '/find',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })
            .state ('soulmate.categoryFind', {
            	url: '/find-by-category/:category/:value',
            	templateUrl: 'view/soulmate/landing-search-category.html'+versionString,
            	controller: "soulmateSearchByCatController"
            })
            /*.state ('soulmate.city', {
            	url: '/city/:city',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })
            .state ('soulmate.employee.sector', {
            	url: '/employee-sector/:employeeSector',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })
            .state ('soulmate.religion', {
            	url: '/religion/:religion/',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })
            .state ('soulmate.physical.status', {
            	url: '/physical-status/:physicalStatus',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })
            .state ('soulmate.marital.status', {
            	url: '/marital-status/:maritalStatus',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })
            .state ('soulmate.profile.age.reigion.cast', {
            	url: '/:gender/:ageFrom/:ageTo/:religion/:cast',
            	templateUrl: 'view/soulmate/landing-search-profile-list.html'+versionString
            })*/
            
            //------------------------------
            // Activity
            //------------------------------

            .state ('activity', {
                url: '/activity',
                abstract: true,
                templateUrl: 'view/activity/common.html'+versionString,
                controller: "bodyController"
            })
			
            .state ('activity.profile-visitor', {
                url: '/profile-visitor',
                templateUrl: 'view/activity/kp_profile-visitor.html'+versionString
            })

            .state ('activity.profile-seen', {
                url: '/profile-seen',
                templateUrl: 'view/activity/kp_profile-seen.html'+versionString
            })

            .state ('activity.interest-sent', {
                url: '/interest-sent',
                templateUrl: 'view/activity/kp_interest-sent.html'+versionString
            })

            .state ('activity.interest-received', {
                url: '/interest-received',
                templateUrl: 'view/activity/kp_interest-received.html'+versionString
            })

            .state ('activity.mutual-interest', {
                url: '/mutual-interest',
                templateUrl: 'view/activity/kp_mutual-interest.html'+versionString
            })

            .state ('activity.shortlisted-profiles', {
                url: '/shortlisted-profiles',
                templateUrl: 'view/activity/kp_shortlisted-profile.html'+versionString
            })

            .state ('activity.likes-you', {
                url: '/likes-you',
                templateUrl: 'view/activity/kp_like-you.html'+versionString
            })
        
		
            //------------------------------
            // Inbox
            //------------------------------

            /* .state ('inbox', {
                url: '/inbox/messages',
                templateUrl: 'view/inbox/kp_received-messages.html'+versionString
            }) */

            .state ('inbox', {
                url: '/inbox',
                templateUrl: 'view/inbox/common.html'+versionString,
                controller: "inboxController"
            })

            .state ('inbox.sent', {
                url: '/sent-massages',
                templateUrl: 'view/inbox/kp_sent-messages.html'+versionString
            })

            .state ('inbox.received', {
                url: '/received-massages',
                templateUrl: 'view/inbox/kp_received-messages.html?v=1'
            });

            $locationProvider.html5Mode(true);
    });
