var app = angular.module('app', ['$ghana', 'ngCookies']);

app.controller('changeemailcontroller', function($scope, $rest, $cookies) {
    var id = $cookies.get('logedUserId');
    
    $scope.changeyouremail = function() {
        var url = 'user/' + id;
        $rest.get(url).then(function(response) {
            var user = response.data;

            var token = user.emailVerificationToken;
            var key = user._id;
            var newEmail = $scope.newemail;

            if (user.email == $scope.currentemail) {
            	
            	 var token = randString(20);
            	 
            	 var data = {
                     "emailVerificationToken": token,
                     "newEmail": newEmail
                 }
            	 $rest.patch(url, data).then(function(response) {
                 }, function(error) {
                     alert(JSON.stringify(error));
                 });

                var data = {
                    "mailSubject": "Please confirm your email address",
                    "mailTo": [
                        newEmail
                    ],
                    "templateName": "confirm-changed-email",
                    "templateValues": {
                        "username": user.email,
                        "email": newEmail, // Note new email
                        "token" : token,
                        "_id" : key,
                        "flow" : "change-email"
                    }
                }

                $rest.post('_email_templates/send-email-use-db-template', data).then(function(response) {
                    alert('Please check your email & confrm email new email address.')
                }, function(error) {
                    alert(JSON.stringify(error));
                });

            } else {
                alert('Please enter correct current email address.');
            }
        }, function(error) {
            alert(JSON.stringify(error));
        });

    }

});