 var app = angular.module('app', ['$ghana', 'ngCookies']);
 app.controller('confirmAccount', function($scope, $rest, $cookies) {
     var token = getParameterByName('token', window.location); //location.search.split('id=')[1];
     var userEmail = getParameterByName('email', window.location);
     var userId = getParameterByName('key', window.location);
     $scope.user = {};
     $scope.confirmAccount = function() {
             var userurl = 'user/' + userId;
             $rest.get(userurl).success(function(response) {
                 var user = response;
                 $scope.user = user;
                 alert("$scope.user " + JSON.stringify($scope.user));
                 // alert(JSON.stringify(user));
                 var url = 'user/confirm-registration/' + token;
                 var data = {};

                 $rest.get(url).then(function(response) {
                         var data = {
                             "mailSubject": "Welcome to Kandapohe.com...",
                             "mailTo": [
                                 user.email
                             ],
                             "templateName": "registration-confirm",
                             "templateValues": {
                                 "username": user.email
                             }
                         }

                         $rest.post('_email_templates/send-email-use-db-template', data).then(function(response) {
                             $rest.get('master_admin/view').success(function(response) {
                                     var admin = response;


                                     for (var x in admin) {
                                         if (admin.hasOwnProperty(x)) {
                                             var data = {

                                                 "mailSubject": "Admin Varification...",
                                                 "mailTo": [
                                                     admin[x].email
                                                 ],
                                                 "templateName": "user-information",
                                                 "templateValues": {
                                                     "username": user.email,
                                                     "firstName": user.firstName,
                                                     "lastName": user.lastName,
                                                     "gender": user.gender,
                                                     "email": user.email,
                                                     "religion": user.religion.name,
                                                     "cast": user.cast.name,
                                                     "day": user.day.name,
                                                     "month": user.month.name,
                                                     "year": user.year.name,
                                                     "marital_status": user.marital_status.name,
                                                     "_id": user._id
                                                 }
                                             }
                                             $rest.post('_email_templates/send-email-use-db-template', data).then(function(response) {
                                                 setTimeout(function() {
                                                     window.location = "/"
                                                 }, 0);
                                             });
                                         }
                                     }




                                 },
                                 function(error) {});
                         });

                     },
                     function(error) {});

             }); //eof 1st get

         } //eof function

 }); //eof controller


 function getParameterByName(name, url) {
     if (!url) url = window.location.href;
     name = name.replace(/[\[\]]/g, "\\$&");
     var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
         results = regex.exec(url);
     if (!results) return null;
     if (!results[2]) return '';
     return decodeURIComponent(results[2].replace(/\+/g, " "));
 }