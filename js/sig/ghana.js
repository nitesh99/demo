//var servicePath = "http://192.168.0.7/mongo/rest/api/v1/dbs/kandapohe/";
//var servicePath = "http://localhost/mongo/rest/api/v1/dbs/kandapohe/";

//var servicePath = "http://192.168.0.5/mongo/rest/api/v1/dbs/kandapohe/";
var servicePath = "http://localhost:1212/mongo/rest/api/v1/dbs/travel/";



//var servicePath = "http://localhost/mongo/rest/api/v1/dbs/kandapohe/";

//var authDomain = "kandapohe.spreaditguru.com";
var authDomain = "localhost:1212";
//var authDomain = "kandapohe.zebrathoughts.com";


var config = {
    authdomain: authDomain,
    databaseurl: servicePath,
    apikey: "AIzaSyDz5cv5IIkDWawWm2bTGzJkc2AxQ7y5Gho",
    'Content-Type': 'application/json'
};

var $ghana = angular.module('$ghana', []);

$ghana.factory('$rest', function($http, $q, $log) {
    var $rest = {
        get: function(path, successHandler, errorHandler) {
            var deferred = $q.defer();
            path = servicePath + path;

            return $http.get(path, { headers: config }).success(function(response) {
                deferred.resolve(response);
            }).error(function(http, status, fnc, httpObj) {
                console.log('GET failed.', http, status, httpObj);
            });

            /*
             .then(function(response, status, headers, config) {
                $log.debug("Success GET API call -> " + path);
                if (successHandler && response)
                    return successHandler.call(this, response.data, status, headers, config);
                else
                    return response;
            }, function(error) {
                $log.error("Failed GET API call -> " + path + ", error code: " + JSON.stringify(error));
                throw {
                    success: false,
                    error: error
                };
            });*/
        },

        post: function(path, data, successHandler, errorHandler) {
            path = servicePath + path;
            return $http({
                method: 'POST',
                url: path,
                headers: config,
                data: data //encodeURIComponent(JSON.stringify(data))
            }).then(function(response) {
                $log.debug("Success POST API call -> " + path + " with supplied data = " + JSON.stringify(data));
                if (successHandler && response)
                    return successHandler.call(this, response.data);
                else
                    return response;
            }, function(error) {
                $log.error("Failed GET API call -> " + path + ", error code: " + JSON.stringify(error));
                throw {
                    success: false,
                    error: error
                };
            });
        },

        put: function(path, data, successHandler, errorHandler) {
            if (data == undefined) {
                data = {};
            }
            path = servicePath + path;
            return $http({
                method: 'PUT',
                url: path,
                data: data, //encodeURIComponent(JSON.stringify(data))
                headers: config,
            }).then(function(response) {
                $log.debug("Success PUT API call -> " + path + " with supplied data = " + JSON.stringify(data));
                if (successHandler && response)
                    return successHandler.call(this, response.data.data);
                else
                    return response;
            }, function(error) {
                $log.error("Failed GET API call -> " + path + ", error code: " + JSON.stringify(error));
                throw {
                    success: false,
                    error: error
                };
            });
        },

        patch: function(path, data, successHandler, errorHandler) {
            if (data == undefined) {
                data = {};
            }
            path = servicePath + path;
            return $http({
                method: 'PATCH',
                url: path,
                data: data,
                headers: config,
            }).then(function(response) {
                $log.debug("Success PUT API call -> " + path + " with supplied data = " + JSON.stringify(data));
                if (successHandler && response)
                    return successHandler.call(this, response.data.data);
                else
                    return response;
            }, function(error) {
                $log.error("Failed GET API call -> " + path + ", error code: " + JSON.stringify(error));
                throw {
                    success: false,
                    error: error
                };
            });
        },

        del: function(path, successHandler, errorHandler) {
            path = servicePath + path;
            return $http({
                method: 'DELETE',
                url: path,
                headers: config,
            }).then(function(response) {
                $log.debug("Success GET API call -> " + path);
                if (successHandler && response)
                    return successHandler.call(this, response.data.data);
                else
                    return response;
            }, function(error) {
                $log.error("Failed GET API call -> " + path + ", error code: " + JSON.stringify(error));
                throw {
                    success: false,
                    error: error
                };
            });
        }
    };
    return $rest;
});


$ghana.factory('$msg', function($ionicPopup) {
    var $msg = {
        info: function(title, msg) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: msg
            });

            alertPopup.then(function(res) {
                console.log('Thank you for not eating my delicious ice cream cone');
            });

            // An elaborate, custom popup
            /*var myPopup = $ionicPopup.show({
              template: '<input type="password" ng-model="data.wifi">',
              title: 'Enter Wi-Fi Password',
              subTitle: 'Please use normal things',
              buttons: [
                { text: 'Cancel' },
                {
                  text: '<b>Save</b>',
                  type: 'button-positive',
                  onTap: function(e) { }
                }
              ]
            });*/

            //toastr.info('Are you the 6 fingered man?');
        },
        error: function(msg) {
            alert("Failed");
        }
    };
    return $msg;
});



/*
var QueryBuilder = function() {
    var query = {
        "condition": [],
        "sortAscending": true,
        "sortOn": "loginUserFirstName"
    };

    var condition = {
        "isOr": false,
        "searchpath": "",
        "values": []
    };

    var search = function(searchPath) {
        var condition = {
            "isOr": false,
            "searchpath": searchPath,
            "values": []
        };
        //query.condition.push(condition);
        return this;
    };
    search.values = function() {
        this.add = function(searchValue) {
            alert('here..');
        }
    };

    return {
        search: function(searchpath) {
            condition.searchpath = searchpath;
            return this;
        },
        values: function(value) {

            return this;
        },
        build: function() {
            return "A is: " + a + ", B is: " + b;
        }
    };
};

var condition = {
    "condition": [{
        "isOr": false,
        "searchpath": "email",
        "values": [
            $scope.user.email
        ]
    }, {
        "isOr": false,
        "searchpath": "password",
        "values": [
            $scope.user.password
        ]
    }],
    "sortAscending": true,
    "sortOn": "loginUserFirstName"
}; */

//var query = QueryBuilder.search('').values().add().add().add().queyr().sortBy('').desc(); or().search().values().add()

//var query = QueryBuilder.searchIn('').values().add().add().add()