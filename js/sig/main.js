/*materialAdmin
        .directive("ajaxCloak", ['$interval', '$http', function ($interval, $http) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    let stop = $interval(() => {
                        if ($http.pendingRequests.length === 0) {
                            $interval.cancel(stop);
                            attrs.$set("ajaxCloak", undefined);
                            element.removeClass("ajax-cloak");
                        }
                    }, 100);

                }
            };
        }]);*/

materialAdmin
    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('materialadminCtrl', function($log, $timeout, $state, $scope, growlService, 
    		$controller, $rootScope, $location, $localStorage, $window, $cookies){
    	
    	$rootScope.tmpData = {};
    	$rootScope.tmpData.social_url = "";

    	$rootScope.masterData = {};
    	$rootScope.masterData.states = [];
	    $rootScope.masterData.districts = [];
	    $rootScope.masterData.cities = [];

	    $rootScope.masterData.employmentStates = [];
	    $rootScope.masterData.employmentDistricts = [];
	    $rootScope.masterData.employmentCities = [];

	    $rootScope.masterData.residentStates = [];
	    $rootScope.masterData.residentDistricts = [];
	    $rootScope.masterData.residentCities = [];
	    
        $rootScope.masterData.AMPM = ["AM", "PM"];
        $rootScope.masterData.namePrefix = ["Mr", "Mrs", "Late"];
        
	    $scope.employment = {};
	    $scope.employment.state = {};
	    $scope.employment.district = {};
	    $scope.employment.city = {};
	    
	    $rootScope.user = {};
	    
	    $rootScope.landingSearch = {};
	    
	    $rootScope.toState = {};
	    $rootScope.fromState = {};
	    
	    $rootScope.unreadMessageCount = 0;
	    
	    $rootScope.completeProfileIndicator = "";
	    $scope.completeProfileArrIndicator = [];
	    
	   /* $rootScope.$on('$stateChangeEnd', function(event, toState, toParams, fromState, fromParams){
	    	var userId = $cookies.get('logedUserId');
	    	
	    	if(toState.name === 'home' && userId != undefined){ 
	    		$state.go("dashboard.default");
            }
	    });*/
	    
	    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
	    	$rootScope.toState = toState;
	    	$rootScope.fromState = fromState;
	    	
	    	if(toState.url !== undefined && toState.url.includes("home")){
	    		$rootScope.landingSearch = {};
	    		$localStorage['landingSearch'] = {};
	    	}
	    	
	    	$log.debug('toState.name = '+toState.name+' & fromState.name = '+fromState.name);
	    	if((toState.name === 'search.advanced' && fromState.name === 'search.basic') || (toState.name === 'search.basic' && fromState.name === 'search.advanced')){
	    		$scope.searchResult = []; // Init needed
	    		$scope.search = {};
	    		
	    		$rootScope.landingSearch = {};
	    		$localStorage['landingSearch'] = {};
	    		$localStorage['searchModel'] = {};
	    	}
	    	
	    	var userId = $cookies.get('logedUserId');
	    	
	    	var loginStates = ["settings", "settings.account-settings", "settings.privacy-settings", "settings.delete-account", "settings.contact-filter", "settings.alert-settings",
	    	                   "registration.basic-information", "registration.education-profession", "registration.lifestyle-appearance", "registration.family-details", "registration.about-yourself",
	    	                   "registration.mobile-verified", "registration.profile-photo", "registration.add-kyc-document", "registration.partner-preference", 
	    	                   "dashboard", "dashboard.default", "dashboard.all-mutual-interest", "dashboard.all-matching-profiles", "dashboard.all-profile-visitors", 
	    	                   "profile", "profile.profile-view", "profile.view-kundali", "profile.profile-edit", "profile.mobile-verified", "profile.kyc-upload", "profile.update-profile-photo", 
	    	                   "profile.update-partner-preference", "profile.search", "profile.view", 
	    	                   "search", "search.basic", "search.advanced",
	    	                   "activity", "activity.profile-visitor", "activity.profile-seen", "activity.interest-sent", "activity.interest-received", "activity.shortlisted-profiles", "activity.likes-you",
	    	                   "inbox", "inbox.sent", "inbox.received"];
	    	
	    	$log.debug('materialadminCtrl $stateChangeStart fromState = '+JSON.stringify(fromState)+' toState = '+JSON.stringify(toState));
	    	if((userId === undefined || userId === '') && loginStates.indexOf(toState.name) !== -1 && ((location.pathname).indexOf('/admin/') < 0)){
	    			$log.debug("Checking user session userId = "+userId);
	    			$log.debug("User is not logged in .. jump to logout toState.name = "+toState.name);
	    			$rootScope.logout('forced');
	    	}
    	
	    	
	    	/*var currentPage = window.location.href;
	    	if(currentPage.includes("home") && (userId !== undefined && $rootScope.user._id !== undefined)){
	    		$state.go("dashboard");
	    	}*/
	    	
	    	$window.scrollTo(0, 0);
		});
	    
	    /*$rootScope.user.employment_sector = {};
	    $rootScope.user.marital_status = {};
	    $rootScope.user.spectacle = {};*/
	    
	    //Cookies.remove('regStepCompleted');

	    $rootScope.logout = function(flow) {
	    	 if('payment' !== flow){
			    	// alert('sending to base path...');
		    	   	setTimeout(function() {
		    	   		if(flow === 'forced'){
		    	   			sweetAlertInitialize();
		    	   			swal({title: "Login session has been expired, Please login again.",type: "error"});
		    	   		}
		    	   		
		    	   		$state.go("home");
		    	   	}, 0);
		        }
	    	 
	    	//$localStorage.$reset();
	    	
	    	var tableName = "user";
	    	var storageName = tableName + "view";
	    	var countStorageNameMale = tableName + "recordcount" + 'Male';
	    	var countStorageNameFemale = tableName + "recordcount" + 'Female';
	    	
	    	$localStorage[countStorageNameMale] = undefined;
	    	$localStorage[countStorageNameFemale] = undefined;
	    	
	    	$localStorage['landingSearch'] = undefined;
	    	$localStorage['profile_photos'] = undefined;
	        
	    	storageName = "allSoulmate" + "view";
	        $localStorage[storageName] = undefined;
	        
	        tableName = "user";
	        var countStorageName = tableName + "recordcount";
	        $localStorage[countStorageName] = undefined;
	        
	        tableName = "user";
	        storageName = tableName + "view";
	    	$localStorage[storageName] = undefined;
	        
	        /*for (var prop in $rootScope) {
        	   if (typeof $rootScope[prop] !== 'function' && prop.indexOf('$') == -1 && prop.indexOf('$$') == -1) {
        	      delete $rootScope[prop];
        	   }
        	} */
	        
	        $rootScope.user = {};
	        $rootScope.logedUserName = 'Login';
	        removeUserCookies($state, $cookies);
	        
	        /** Clear user specific collections **/
	        $rootScope.profilePics = [];
	        $rootScope.allUsers = [];
		    $rootScope.maleUsers = [];
		    $rootScope.femaleUsers = [];
		    $rootScope.matchingProfiles = [];
		    $rootScope.recentVisitor = [];
		    $rootScope.mutualInterest = [];
		    $rootScope.profileILikesMapping = [];
		    $rootScope.profileLikesMeMapping = [];
		    $rootScope.profileShortlistedMapping = [];
		    $rootScope.profileInterestedInMapping = [];
		    $rootScope.profileInterestReceivedMapping = [];
		    $rootScope.userNotificationsMapping = [];
		    $rootScope.profileSeenMapping = [];
		    $rootScope.profileVisitorMapping = [];
		    $rootScope.profileSeen = [];
		    $rootScope.profileInterestSent = [];
		    $rootScope.profileInterestReceived = [];
		    $rootScope.profileShortlisted = [];
		    $rootScope.profileVisitor = [];
		    $rootScope.profileLikesMe = [];
		    $rootScope.profileILikes = [];
		    $rootScope.profilePics = [];
		    $rootScope.userNotifications = [];
		    
		    $rootScope.landingSearch = {};
		    
		    storageName = "userview";
		    $localStorage[storageName] = undefined;
		    
		    $rootScope.sendMessageToUser = {};
		    
	    };
	    
	    $rootScope.allUsers = [];
	    $rootScope.matchingProfiles = [];
	    $rootScope.recentVisitor = [];
	    $rootScope.mutualInterest = [];
	    
	    /** Required for view profile **/
	    $rootScope.profileILikesMapping = [];
	    $rootScope.profileLikesMeMapping = [];
	    $rootScope.profileShortlistedMapping = [];
	    $rootScope.profileInterestedInMapping = [];
	    $rootScope.profileInterestReceivedMapping = [];
	    $rootScope.profileSeenMapping = [];
	    $rootScope.profileVisitorMapping = [];

	    $rootScope.profileSeen = [];
	    $rootScope.profileInterestSent = [];
	    $rootScope.profileInterestReceived = [];
	    $rootScope.profileShortlisted = [];
	    $rootScope.profileVisitor = [];
	    $rootScope.profileLikesMe = [];
	    
	    $rootScope.profilePics = [];
	    
        //Welcome Message
        //growlService.growl('Welcome back Mallinda!', 'inverse')
        
        // Detact Mobile Browser
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        };

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');
        
        // For Mainmenu Active Class
        this.$state = $state;    
        
        //Close sidebar on click
        this.sidebarStat = function(event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        };
        
        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;
        
        this.lvSearch = function() {
            this.listviewSearchStat = true; 
        };
        
        //Listview menu toggle in small screens
        this.lvMenuStat = false;
        
        //Blog
        this.wallCommenting = [];
        
        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'blue';

        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
        ];

        this.skinSwitch = function (color) {
            this.currentSkin = color;
        };
        
        angular.extend(this, $controller('ModalDemoCtrl', {$scope: $scope}));
    })


    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', function($timeout, messageService){


        // Top Search
        this.openSearch = function(){
            angular.element('#header').addClass('search-toggled');
            angular.element('#top-search-wrap').find('input').focus();
        };

        this.closeSearch = function(){
            angular.element('#header').removeClass('search-toggled');
        };
        
        // Get messages and notification for header
        this.img = messageService.img;
        this.user = messageService.user;
        this.user = messageService.text;

        this.messageResult = messageService.getMessage(this.img, this.user, this.text);


        //Clear Notification
        this.clearNotification = function($event) {
            $event.preventDefault();
            
            var x = angular.element($event.target).closest('.listview');
            var y = x.find('.lv-item');
            var z = y.size();
            
            angular.element($event.target).parent().fadeOut();
            
            x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
            x.find('.grid-loading').fadeIn(1500);
            var w = 0;
            
            y.each(function(){
                var z = $(this);
                $timeout(function(){
                    z.addClass('animated fadeOutRightBig').delay(1000).queue(function(){
                        z.remove();
                    });
                }, w+=150);
            })
            
            $timeout(function(){
                angular.element('#notifications').addClass('empty');
            }, (z*150)+200);
        }
        
        // Clear Local Storage
        this.clearLocalStorage = function() {
            
            //Get confirmation, if confirmed clear the localStorage
            swal({   
                title: "Are you sure?",   
                text: "All your saved localStorage values will be removed",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#F44336",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                localStorage.clear();
                swal("Done!", "localStorage is cleared", "success"); 
            });
            
        }
        
        //Fullscreen View
        this.fullScreen = function() {
            //Launch
            function launchIntoFullscreen(element) {
                if(element.requestFullscreen) {
                    element.requestFullscreen();
                } else if(element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if(element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if(element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            //Exit
            function exitFullscreen() {
                if(document.exitFullscreen) {
                    document.exitFullscreen();
                } else if(document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if(document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            }
            else {
                launchIntoFullscreen(document.documentElement);
            }
        }
    
    })



    // =========================================================================
    // Best Selling Widget
    // =========================================================================

    .controller('bestsellingCtrl', function(bestsellingService){
        // Get Best Selling widget Data
        this.img = bestsellingService.img;
        this.name = bestsellingService.name;
        this.range = bestsellingService.range; 
        
        this.bsResult = bestsellingService.getBestselling(this.img, this.name, this.range);
    })

 
    // =========================================================================
    // Todo List Widget
    // =========================================================================

    .controller('todoCtrl', function(todoService){
        
        //Get Todo List Widget Data
        this.todo = todoService.todo;
        
        this.tdResult = todoService.getTodo(this.todo);
        
        //Add new Item (closed by default)
        this.addTodoStat = false;
    })


    // =========================================================================
    // Recent Items Widget
    // =========================================================================

    .controller('recentitemCtrl', function(recentitemService){
        
        //Get Recent Items Widget Data
        this.id = recentitemService.id;
        this.name = recentitemService.name;
        this.parseInt = recentitemService.price;
        
        this.riResult = recentitemService.getRecentitem(this.id, this.name, this.price);
    })


    // =========================================================================
    // Recent Posts Widget
    // =========================================================================
    
    .controller('recentpostCtrl', function(recentpostService){
        
        //Get Recent Posts Widget Items
        this.img = recentpostService.img;
        this.user = recentpostService.user;
        this.text = recentpostService.text;
        
        this.rpResult = recentpostService.getRecentpost(this.img, this.user, this.text);
    })


    //=================================================
    // Profile
    //=================================================

    .controller('profileCtrl', function($sce, growlService){
        

    	  this.getHtml = function(html){
    	        return $sce.trustAsHtml(html);
    	    };
    	  
    	  this.user = {};
    	  this.user.gender = {};
    	  this.genders = [ // Taken from https://gist.github.com/unceus/6501985
    	    {name: 'Male', code: 'M'},
    	    {name: 'Female', code: 'F'}
    	  ];

        //Get Profile Information from profileService Service
        
        //User
        this.profileSummary = "Sed eu est vulputate, fringilla ligula ac, maximus arcu. Donec sed felis vel magna mattis ornare ut non turpis. Sed id arcu elit. Sed nec sagittis tortor. Mauris ante urna, ornare sit amet mollis eu, aliquet ac ligula. Nullam dolor metus, suscipit ac imperdiet nec, consectetur sed ex. Sed cursus porttitor leo.";
    
        this.tryMe = "--tryMe--";
        this.fullName = "Mallinda Hollaway";
        this.gender = "female";
        this.birthDay = "23/06/1988";
        this.martialStatus = "Single";
        this.mobileNumber = "00971123456789";
        this.emailAddress = "malinda.h@gmail.com";
        this.twitter = "@malinda";
        this.twitterUrl = "twitter.com/malinda";
        this.skype = "malinda.hollaway";
        this.addressSuite = "44-46 Morningside Road";
        this.addressCity = "Edinburgh";
        this.addressCountry = "Scotland";

        //Edit
        this.editSummary = 0;
        this.editInfo = 0;
        this.editContact = 0;
    
        
        this.submit = function(item, message) {            
            if(item === 'profileSummary') {
                this.editSummary = 0;
            }
            
            if(item === 'profileInfo') {
                this.editInfo = 0;
            }
            
            if(item === 'profileContact') {
                this.editContact = 0;
            }
            
            growlService.growl(message+' has updated Successfully!', 'inverse'); 
        }

    })



    //=================================================
    // LOGIN
    //=================================================

    .controller('loginCtrl', function(){
        
        //Status
    
        this.login = 1;
        this.register = 0;
        this.forgot = 0;
    })


    //=================================================
    // CALENDAR
    //=================================================
    
    .controller('calendarCtrl', function($modal){
    
        //Create and add Action button with dropdown in Calendar header. 
        this.month = 'month';
    
        this.actionMenu = '<ul class="actions actions-alt" id="fc-actions">' +
                            '<li class="dropdown" dropdown>' +
                                '<a href="" dropdown-toggle><i class="zmdi zmdi-more-vert"></i></a>' +
                                '<ul class="dropdown-menu dropdown-menu-right">' +
                                    '<li class="active">' +
                                        '<a data-calendar-view="month" href="">Month View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="basicWeek" href="">Week View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="agendaWeek" href="">Agenda Week View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="basicDay" href="">Day View</a>' +
                                    '</li>' +
                                    '<li>' +
                                        '<a data-calendar-view="agendaDay" href="">Agenda Day View</a>' +
                                    '</li>' +
                                '</ul>' +
                            '</div>' +
                        '</li>';

            
        //Open new event modal on selecting a day
        this.onSelect = function(argStart, argEnd) {            
            var modalInstance  = $modal.open({
                templateUrl: 'addEvent.html',
                controller: 'addeventCtrl',
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    calendarData: function() {
                        var x = [argStart, argEnd];
                        return x;
                    }
                }
            });
        }
    })

    //Add event Controller (Modal Instance)
    .controller('addeventCtrl', function($scope, $modalInstance, calendarData){
        
        //Calendar Event Data
        $scope.calendarData = {
            eventStartDate: calendarData[0],
            eventEndDate:  calendarData[1]
        };
    
        //Tags
        $scope.tags = [
            'bgm-teal',
            'bgm-red',
            'bgm-pink',
            'bgm-blue',
            'bgm-lime',
            'bgm-green',
            'bgm-cyan',
            'bgm-orange',
            'bgm-purple',
            'bgm-gray',
            'bgm-black',
        ]
        
        //Select Tag
        $scope.currentTag = '';
        
        $scope.onTagClick = function(tag, $index) {
            $scope.activeState = $index;
            $scope.activeTagColor = tag;
        } 
        
        //Add new event
        $scope.addEvent = function() {
            if ($scope.calendarData.eventName) {

                //Render Event
                $('#calendar').fullCalendar('renderEvent',{
                    title: $scope.calendarData.eventName,
                    start: $scope.calendarData.eventStartDate,
                    end:  $scope.calendarData.eventEndDate,
                    allDay: true,
                    className: $scope.activeTagColor

                },true ); //Stick the event

                $scope.activeState = -1;
                $scope.calendarData.eventName = '';     
                $modalInstance.close();
            }
        }
        
        //Dismiss 
        $scope.eventDismiss = function() {
            $modalInstance.dismiss();
        }
    })

    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('formCtrl', function(){
    
        //Input Slider
        this.nouisliderValue = 4;
        this.nouisliderFrom = 25;
        this.nouisliderTo = 80;
        this.nouisliderRed = 35;
        this.nouisliderBlue = 90;
        this.nouisliderCyan = 20;
        this.nouisliderAmber = 60;
        this.nouisliderGreen = 75;
    
        //Color Picker
        this.color = '#03A9F4';
        this.color2 = '#8BC34A';
        this.color3 = '#F44336';
        this.color4 = '#FFC107';
    })


    // =========================================================================
    // PHOTO GALLERY
    // =========================================================================

    .controller('photoCtrl', function(){
        
        //Default grid size (2)
        this.photoColumn = 'col-md-2';
        this.photoColumnSize = 2;
    
        this.photoOptions = [
            { value: 2, column: 6 },
            { value: 3, column: 4 },
            { value: 4, column: 3 },
            { value: 1, column: 12 },
        ]
    
        //Change grid
        this.photoGrid = function(size) {
            this.photoColumn = 'col-md-'+size;
            this.photoColumnSize = size;
        }
    
    })


    // =========================================================================
    // ANIMATIONS DEMO
    // =========================================================================
    .controller('animCtrl', function($timeout){
        
        //Animation List
        this.attentionSeekers = [
            { animation: 'bounce', target: 'attentionSeeker' },
            { animation: 'flash', target: 'attentionSeeker' },
            { animation: 'pulse', target: 'attentionSeeker' },
            { animation: 'rubberBand', target: 'attentionSeeker' },
            { animation: 'shake', target: 'attentionSeeker' },
            { animation: 'swing', target: 'attentionSeeker' },
            { animation: 'tada', target: 'attentionSeeker' },
            { animation: 'wobble', target: 'attentionSeeker' }
        ]
        this.flippers = [
            { animation: 'flip', target: 'flippers' },
            { animation: 'flipInX', target: 'flippers' },
            { animation: 'flipInY', target: 'flippers' },
            { animation: 'flipOutX', target: 'flippers' },
            { animation: 'flipOutY', target: 'flippers'  }
        ]
         this.lightSpeed = [
            { animation: 'lightSpeedIn', target: 'lightSpeed' },
            { animation: 'lightSpeedOut', target: 'lightSpeed' }
        ]
        this.special = [
            { animation: 'hinge', target: 'special' },
            { animation: 'rollIn', target: 'special' },
            { animation: 'rollOut', target: 'special' }
        ]
        this.bouncingEntrance = [
            { animation: 'bounceIn', target: 'bouncingEntrance' },
            { animation: 'bounceInDown', target: 'bouncingEntrance' },
            { animation: 'bounceInLeft', target: 'bouncingEntrance' },
            { animation: 'bounceInRight', target: 'bouncingEntrance' },
            { animation: 'bounceInUp', target: 'bouncingEntrance'  }
        ]
        this.bouncingExits = [
            { animation: 'bounceOut', target: 'bouncingExits' },
            { animation: 'bounceOutDown', target: 'bouncingExits' },
            { animation: 'bounceOutLeft', target: 'bouncingExits' },
            { animation: 'bounceOutRight', target: 'bouncingExits' },
            { animation: 'bounceOutUp', target: 'bouncingExits'  }
        ]
        this.rotatingEntrances = [
            { animation: 'rotateIn', target: 'rotatingEntrances' },
            { animation: 'rotateInDownLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInDownRight', target: 'rotatingEntrances' },
            { animation: 'rotateInUpLeft', target: 'rotatingEntrances' },
            { animation: 'rotateInUpRight', target: 'rotatingEntrances'  }
        ]
        this.rotatingExits = [
            { animation: 'rotateOut', target: 'rotatingExits' },
            { animation: 'rotateOutDownLeft', target: 'rotatingExits' },
            { animation: 'rotateOutDownRight', target: 'rotatingExits' },
            { animation: 'rotateOutUpLeft', target: 'rotatingExits' },
            { animation: 'rotateOutUpRight', target: 'rotatingExits'  }
        ]
        this.fadeingEntrances = [
            { animation: 'fadeIn', target: 'fadeingEntrances' },
            { animation: 'fadeInDown', target: 'fadeingEntrances' },
            { animation: 'fadeInDownBig', target: 'fadeingEntrances' },
            { animation: 'fadeInLeft', target: 'fadeingEntrances' },
            { animation: 'fadeInLeftBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInRight', target: 'fadeingEntrances'  },
            { animation: 'fadeInRightBig', target: 'fadeingEntrances'  },
            { animation: 'fadeInUp', target: 'fadeingEntrances'  },
            { animation: 'fadeInBig', target: 'fadeingEntrances'  }
        ]
        this.fadeingExits = [
            { animation: 'fadeOut', target: 'fadeingExits' },
            { animation: 'fadeOutDown', target: 'fadeingExits' },
            { animation: 'fadeOutDownBig', target: 'fadeingExits' },
            { animation: 'fadeOutLeft', target: 'fadeingExits' },
            { animation: 'fadeOutLeftBig', target: 'fadeingExits'  },
            { animation: 'fadeOutRight', target: 'fadeingExits'  },
            { animation: 'fadeOutRightBig', target: 'fadeingExits'  },
            { animation: 'fadeOutUp', target: 'fadeingExits'  },
            { animation: 'fadeOutUpBig', target: 'fadeingExits'  }
        ]
        this.zoomEntrances = [
            { animation: 'zoomIn', target: 'zoomEntrances' },
            { animation: 'zoomInDown', target: 'zoomEntrances' },
            { animation: 'zoomInLeft', target: 'zoomEntrances' },
            { animation: 'zoomInRight', target: 'zoomEntrances' },
            { animation: 'zoomInUp', target: 'zoomEntrances'  }
        ]
        this.zoomExits = [
            { animation: 'zoomOut', target: 'zoomExits' },
            { animation: 'zoomOutDown', target: 'zoomExits' },
            { animation: 'zoomOutLeft', target: 'zoomExits' },
            { animation: 'zoomOutRight', target: 'zoomExits' },
            { animation: 'zoomOutUp', target: 'zoomExits'  }
        ]

        //Animate    
        this.ca = '';
    
        this.setAnimation = function(animation, target) {
            if (animation === "hinge") {
                animationDuration = 2100;
            }
            else {
                animationDuration = 1200;
            }
            
            angular.element('#'+target).addClass(animation);
            
            $timeout(function(){
                angular.element('#'+target).removeClass(animation);
            }, animationDuration);
        }
    
    })

