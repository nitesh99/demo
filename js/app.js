var materialAdmin = angular.module('materialAdmin', ['$ghana', 
    'ngAnimate',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'oc.lazyLoad',
    'nouislider',
		'ngTable',
		'ngRoute'
])
